require "json"
require "set"

def normalize_name(name)
  name.sub(%r{ \(\*\)$}, "")
end

def normalize_name_c(name)
  name.sub(%r{ \(c\)$}, "")
end

def normalize_name_n(name)
  name.sub(%r{ \(n\)$}, "")
end

def format_label(label)
  label.split(":").join("\n:")
end

def dup_node(name)
  if name.end_with?("(*)")
    name_norm = normalize_name(name)
    [
      [name, name_norm]
    ]
  elsif name.end_with?("(c)")
    name_norm = normalize_name_c(name)
    [
      [name, name_norm]
    ]
  elsif name.end_with?("(n)")
    name_norm = normalize_name_n(name)
    [
      [name, name_norm]
    ]
  else
    []
  end
end

def main(src)
  data = JSON.parse(src)

  adj_set = []
  data.each { |d|
    adj_set += d["deps"]
  }

  # # (*) を除去
  # adj_set =
  #   adj_set.map { |from, to|
  #     [
  #       normalize_name(from),
  #       normalize_name(to)
  #     ]
  #   }

  adj_set = adj_set.uniq

  adj_set2 = []
  adj_set.each { |from, to|
    adj_set2 << [from, to]
    # adj_set2 += dup_node(from)
    adj_set2 += dup_node(to)
  }

  node_set = Set.new
  adj_set2.each { |from, to|
    node_set << from
    node_set << to
  }
  nodes = node_set.to_a

  src_node_defs = ""
  nodes.each_with_index { |n, i|
    src_node_defs << "\n" + %Q!  n#{i} [ label = "#{format_label(n)}" ]!
  }

  edges = []
  adj_set2.each{|from, to|
    id_from = nodes.index(from)
    id_to = nodes.index(to)
    edges << %Q!  n#{id_from} -> n#{id_to};!
  }

  src_edge_defs = edges.join("\n")

  puts <<~DOT
digraph graph1 {

  graph [
    rankdir = LR;
  ];

  node [
    fontname = "monospace";
  ];

  #{src_node_defs}

  #{src_edge_defs}
}
  DOT
end

main($stdin.read)
