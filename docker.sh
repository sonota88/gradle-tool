#!/bin/bash

set -o nounset

readonly IMAGE=java-dep-graph:1
readonly CONTAINER_HOME=/root

print_this_dir() {
  (
    cd "$(dirname "$0")"
    pwd
  )
}

cmd_build() {
  docker build \
    --progress=plain \
    -t $IMAGE .
}

cmd_run_i() {
  docker run --rm -i \
    -v "$(print_this_dir):${CONTAINER_HOME}/work" \
    $IMAGE "$@"
}

cmd_run() {
  # docker run --rm -t \
  #   -v "$(pwd):${CONTAINER_HOME}/work" \
  #   $IMAGE "$@"

  docker run --rm -it \
    -v "$(print_this_dir):${CONTAINER_HOME}/work" \
    $IMAGE "$@"
}

cmd="$1"; shift
case $cmd in
  build | b* )
    cmd_build "$@"
;; run-i )
     cmd_run_i "$@"
;; run | r* )
     cmd_run "$@"
;; * )
     echo "invalid command (${cmd})" >&2
     ;;
esac
