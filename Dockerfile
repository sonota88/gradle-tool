FROM ubuntu:22.04

ENV DEBIAN_FRONTEND=noninteractive

# RUN apt-get update \
#   && apt-get install -y --no-install-recommends \
#     curl \
#     ruby \
#   && apt-get clean \
#   && rm -rf /var/lib/apt/lists/*

# --------------------------------

RUN apt-get update

RUN apt-get install -y --no-install-recommends ruby
RUN apt-get install -y --no-install-recommends graphviz

# --------------------------------

ENV HOME=/root

WORKDIR ${HOME}/work

ENV LANG=en_US.UTF-8

# CMD ["bash", "-l"]
