#!/bin/bash

set -o nounset

print_this_dir() {
  (
    cd "$(dirname "$0")"
    pwd
  )
}

readonly __dir__="$(print_this_dir)"
readonly DIR_WORK=/root/work

cd $__dir__

${__dir__}/docker.sh run-i \
  bash run_in.sh
