require "json"

def extract_name(line)
  if line.split(" - ")[0].nil?
    raise "TODO"
  end

  line.chomp.split(" - ")[0]
end

def parse_with_deps(lines)
  lines4 = lines.map{ _1.sub("\\---", "+---") }

  line0, *lines2 = lines4
  name = extract_name(line0)

  stack = []
  stack.push name

  lines3 =
    lines2.map { |line|
      m = line.match(%r{^([|+\- ]+)(.+)})
      if m
        [m[1], m[2]]
      else
        raise line.inspect
      end
    }

  deps = []
  lv_prev = 0

  lines3.each { |head, tail|
    lv = head.size / 5
    delta = lv - lv_prev

    if delta > 0
      ;
    elsif delta < 0
      (-delta + 1).times {
        stack.pop
      }
    else
      stack.pop
    end

    deps << [stack.last, tail]

    stack.push(tail)
    lv_prev = lv
  }

  {
    name: name,
    deps: deps
  }
end

def target_para?(para)
  (
    %r{^No dependencies} =~ para ||
    %r{^\+--- } =~ para ||
    %r{^\\--- } =~ para
  )
end

def parse_para(para)
  lines = para.lines.map { _1.chomp }

  name = extract_name(lines[0])

  if lines[1] == "No dependencies"
    {
      name: name,
      deps: [[name, "__no_deps__"]]
    }
  else
    parse_with_deps(lines)
  end
end

def parse(src)
  # split to paragraphs
  paras = src.split("\n\n")

  paras2 = paras.select { target_para?(_1) }

  names = paras2.map { |para|
    extract_name(para.lines[0])
  }

  root_data =
    {
      name: "__root__",
      deps: names.map { |name|
        ["__root__", name]
      }
    }

  data =
    paras2.map { |para|
      parse_para(para)
    }

  [root_data, *data]
end

if $0 == __FILE__
  src = $stdin.read
  data = parse(src)
  puts JSON.pretty_generate(data)
end
