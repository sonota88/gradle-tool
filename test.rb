require "minitest/autorun"

require_relative "parser"

class TestFoo < Minitest::Test
  def test_no_deps
    src = <<~SRC
      aa
      No dependencies
    SRC

    assert_equal([["aa", nil]], parse(src))
  end

  def test_010
    src = <<~SRC
      aa
      +--- bb
    SRC

    assert_equal(
      [["aa", "bb"]],
      parse(src)
    )
  end

  def test_020
    src = <<~SRC
      aa
      +--- bb
      +--- cc
    SRC

    assert_equal(
      [
        ["aa", "bb"],
        ["aa", "cc"]
      ],
      parse(src)
    )
  end

  def test_030
    src = <<~SRC
      aa
      +--- bb
      |    +--- cc
      +--- dd
    SRC

    assert_equal(
      [
        ["aa", "bb"],
        ["bb", "cc"],
        ["aa", "dd"],
      ],
      parse(src)
    )
  end
end
