#!/bin/bash

print_this_dir() {
  (
    cd "$(dirname "$0")"
    pwd
  )
}

ruby parser.rb > z_deps.json

cat z_deps.json \
  | ruby to_dot.rb \
  > z_dot.dot

dot -Tsvg z_dot.dot
